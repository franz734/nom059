<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('nom059.panelNom');
}); */

//NOM-059
///GET///
Route::get('/nom059', 'Nom059Controller@index');
Route::get('/nom059/listaCombo/{id}', 'Nom059Controller@listaCombo');
Route::get('/nom059/listaComboByGrupo/{id}', 'Nom059Controller@listaComboByGrupo');
Route::get('/nom059/listaComboByFamilia/{id}', 'Nom059Controller@listaComboByFamilia');
Route::get('/nom059/listaComboByGenero/{id}', 'Nom059Controller@listaComboByGenero');
Route::get('/nom059/listaComboByRiesgo/{riesgo}/{reino}/{grupo}/{familia}/{genero}', 'Nom059Controller@listaComboByRiesgo');
Route::get('/nom059/listaComboByEspecie/{id}', 'Nom059Controller@listaComboByEspecie');
Route::get('/nom059/listaComboByEntidad/{id}', 'Nom059Controller@listaComboByEntidad');
Route::get('/nom059/detallesEspecie/{id}', 'Nom059Controller@detallesEspecie');
Route::get('/nom059/listaEspecieByEntidad/{id}', 'Nom059Controller@listaEspecieByEntidad');

///POST///
Route::post('/nom059/filter', 'Nom059Controller@filter');