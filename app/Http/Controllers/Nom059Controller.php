<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reino;
use App\Grupo;
use App\Familia;
use App\Genero;
use App\Especie;
use App\Entidad;
use App\CategoriaRiesgo;
use App\EspecieEntidad;
use View;
use DataTables;

class Nom059Controller extends Controller
{
    /// panel NOM-059 ///
    public function index(){
        $reinos = Reino::all();
        $grupos = Grupo::orderBy('nomGrupo','ASC')->get();
        $familias = Familia::all();
        $generos = Genero::all();
        $especies = Especie::orderBy('especie','ASC')->get();
        $entidades = Entidad::all();
        $categoriasRiesgo = CategoriaRiesgo::all();
        //dd($especies[0]);
        return view('nom059.panelNom', ['reinos'=>$reinos, 'grupos'=>$grupos,
                                        'familias'=>$familias, 'generos'=>$generos,
                                        'especies'=>$especies, 'entidades'=>$entidades,
                                        'categoriasRiesgo'=>$categoriasRiesgo]);
    }
    /// Lista Combo ///
    public function listaCombo($id){
        $familiaArr =[];
        $generoArr = [];
        $categoriaRiesgoArr = [];
        $idsEspecie = [];
        $grupos = Grupo::getGruposByReino($id);
        $especies = Especie::getEspeciesByReino($id);
        //dd($especies[0]);
        foreach($especies as $especie){
            array_push($familiaArr,$especie->idFamilia);
            array_push($generoArr,$especie->idGenero);
            array_push($categoriaRiesgoArr,$especie->idCategoriaRiesgo);
            array_push($idsEspecie,$especie->idEspecie);
        }
        $familiaArr = array_unique($familiaArr);
        $generoArr = array_unique($generoArr);
        $categoriaRiesgoArr  =array_unique($categoriaRiesgoArr);
        $familias = Familia::getFamiliasByReino($familiaArr);
        $generos = Genero::getGenerosByReino($generoArr);        
        $idEdos = getEntidades($idsEspecie);
        $categoriasRiesgo = CategoriaRiesgo::getCategoriasByReino($categoriaRiesgoArr);
        $cad = View::make("nom059.mapa",['idEdos'=>$idEdos])->render();
        $response = [
            'grupos'=>$grupos,
            'especies'=>$especies,
            'familias'=>$familias,
            'generos'=>$generos,
            'riesgos'=>$categoriasRiesgo,
            'vista'=>$cad
        ];
        //dd($response);
        return response()->json($response,200);
    }
    /// Lista combo dado un grupo ///
    public function listaComboByGrupo($id){
        $familiaArr = [];
        $generoArr = [];
        $categoriaRiesgoArr = [];
        $idsEspecie = [];
        $especies = Especie::getEspeciesByGrupo($id);
        foreach($especies as $especie){
            array_push($familiaArr,$especie->idFamilia);
            array_push($generoArr,$especie->idGenero);
            array_push($categoriaRiesgoArr,$especie->idCategoriaRiesgo);
            array_push($idsEspecie,$especie->idEspecie);
        }
        $familiaArr = array_unique($familiaArr);
        $generoArr = array_unique($generoArr);
        $categoriaRiesgoArr  =array_unique($categoriaRiesgoArr);
        $familias = Familia::getFamiliasByReino($familiaArr);
        $generos = Genero::getGenerosByReino($generoArr);
        $idEdos = getEntidades($idsEspecie);
        $categoriasRiesgo = CategoriaRiesgo::getCategoriasByReino($categoriaRiesgoArr);
        $cad = View::make("nom059.mapa",['idEdos'=>$idEdos])->render();
        $response = [            
            'especies'=>$especies,
            'familias'=>$familias,
            'generos'=>$generos,
            'riesgos'=>$categoriasRiesgo,
            'vista'=>$cad
        ];
        //dd($response);
        return response()->json($response,200);
    }
    /// Lista combo dada una familia ///
    public function listaComboByFamilia($id){
        $generoArr = [];
        $categoriaRiesgoArr = [];
        $idsEspecie = [];
        $especies = Especie::getEspeciesByFamilia($id);
        foreach($especies as $especie){            
            array_push($generoArr,$especie->idGenero);
            array_push($categoriaRiesgoArr,$especie->idCategoriaRiesgo);
            array_push($idsEspecie,$especie->idEspecie);
        }        
        $generoArr = array_unique($generoArr);    
        $categoriaRiesgoArr  =array_unique($categoriaRiesgoArr);    
        $generos = Genero::getGenerosByReino($generoArr);
        $idEdos = getEntidades($idsEspecie);
        $categoriasRiesgo = CategoriaRiesgo::getCategoriasByReino($categoriaRiesgoArr);
        $cad = View::make("nom059.mapa",['idEdos'=>$idEdos])->render();
        $response = [            
            'especies'=>$especies,            
            'generos'=>$generos,
            'riesgos'=>$categoriasRiesgo,
            'vista'=>$cad
        ];
        //dd($response);
        return response()->json($response,200);
    }
    /// Lista combo dado un genero ///
    public function listaComboByGenero($id){
        $categoriaRiesgoArr = [];
        $idsEspecie = [];
        $especies = Especie::getEspeciesByGenero($id);  
        foreach($especies as $especie){                        
            array_push($categoriaRiesgoArr,$especie->idCategoriaRiesgo);
            array_push($idsEspecie,$especie->idEspecie);
        }       
        $categoriaRiesgoArr  =array_unique($categoriaRiesgoArr);
        $idEdos = getEntidades($idsEspecie);
        $categoriasRiesgo = CategoriaRiesgo::getCategoriasByReino($categoriaRiesgoArr);
        $cad = View::make("nom059.mapa",['idEdos'=>$idEdos])->render();
        $response = [            
            'especies'=>$especies,
            'riesgos'=>$categoriasRiesgo,
            'vista'=>$cad            
        ];
        //dd($response);
        return response()->json($response,200);
    }

    /// Lista combo dado dada un acategoria de riesgo ///
    public function listaComboByRiesgo($a,$b,$c,$d,$e){
        $param  = [
            'riesgo' => $a,
            'reino' => $b,
            'grupo' => $c,
            'familia' => $d,
            'genero' => $e
        ];
        //dd($param);
        $especies = Especie::getEspeciesByCategoriaRiesgo($param);
        $response = [            
            'especies'=>$especies                       
        ];
        //dd($response);
        return response()->json($response,200);
    }

    /// Lista combo dada un aespecie ///
    public function listaComboByEspecie($id){
        $entidadesArr =[]; 
        $idsEspecie = []; 
        $categoriaRiesgoArr = [];
        $especie = Especie::where('idEspecie',$id)->first(); 
        array_push($idsEspecie,$especie->idEspecie);         
        $categoriaRiesgo = CategoriaRiesgo::where('idCategoriaRiesgo',$especie->idCategoriaRiesgo)->get(); 
        $idEdos = getEntidades($idsEspecie);  
        $cad = View::make("nom059.mapa",['idEdos'=>$idEdos])->render();   
        $entidades = EspecieEntidad::getEntidadesByEspecie($id);
        foreach($entidades as $entidad){
            $entidadIn = $entidad->entidadRel;
            array_push($entidadesArr,$entidadIn);
        }
        $response = [            
            'entidades'=>$entidadesArr,
            'riesgos'=>$categoriaRiesgo,
            'especie'=>$especie,
            'vista'=>$cad               
        ];        
        return response()->json($response,200);
    }

    /// Lista combo dada una entidad ///
    public function listaComboByEntidad($id){
        //dd($id);
        $idEdos =[];
        $entidad = Entidad::where('idEntidad',$id)->first();
        array_push($idEdos,$entidad->cveEntidad);
        $cad = View::make("nom059.mapa",['idEdos'=>$idEdos])->render();
        $response = [                        
            'vista'=>$cad               
        ];        
        return response()->json($response,200);
    }

    /// Filtro con boton ///
    public function filter(Request $request){ 
        $complemento = ''; 
        if($request->has('idEntidad')){ //Filtro con Entidad
            //dd('cambio de consulta');
            $idEntidad = $request->idEntidad;            
            $consulta = 'SELECT especie.*, especieEntidad.idEntidad, ct_entidad.nomEntidad, 
                         ct_reino.nomReino, ct_grupo.nomGrupo,
                         ct_familia.nomFamilia, ct_genero.nomGenero,ct_categoriaRiesgo.categoriaRiesgo
                         FROM especieEntidad JOIN especie 
                         JOIN ct_entidad 
                         JOIN ct_reino 
                         JOIN ct_genero JOIN ct_familia
                         JOIN ct_grupo JOIN ct_categoriaRiesgo
                         WHERE especieEntidad.idEspecie = especie.idEspecie 
                         AND especieEntidad.idEntidad = ct_entidad.idEntidad 
                         AND especie.idEspecie IN ';
            $in = '(SELECT especie.idEspecie FROM especie WHERE ';
            $request = (object)$request->except('idEntidad');            
            $n  = sizeof((array)$request);                    
            //dd($n);
            if(empty((array)$request)){
                $consulta = "SELECT especie.*, especieEntidad.idEntidad, ct_entidad.nomEntidad,
                             ct_reino.nomReino, ct_grupo.nomGrupo,
                             ct_familia.nomFamilia, ct_genero.nomGenero,ct_categoriaRiesgo.categoriaRiesgo
                             FROM especieEntidad JOIN especie 
                             JOIN ct_entidad 
                             JOIN ct_reino 
                             JOIN ct_genero JOIN ct_familia
                             JOIN ct_grupo JOIN ct_categoriaRiesgo
                             WHERE especieEntidad.idEspecie = especie.idEspecie
                             AND especieEntidad.idEntidad = ct_entidad.idEntidad 
                             AND especieEntidad.idEntidad = $idEntidad";
                $complemento = ' AND ct_reino.idReino = especie.idReino 
                                AND ct_grupo.idGrupo = especie.idGrupo 
                                AND ct_familia.idFamilia = especie.idFamilia 
                                AND ct_genero.idGenero = especie.idGenero 
                                AND ct_categoriaRiesgo.idCategoriaRiesgo = especie.idCategoriaRiesgo';
            }
            else{
                if($n == 1){
                    $u = '';
                } 
                elseif($n > 1){
                    $u = ' AND ';                
                }
                $campos = array_keys((array)$request);
                $valores = array_values((array)$request);
                for($i=0; $i<=$n-1; $i++){
                    if($i == $n-1){
                        $in.=$campos[$i].'='.$valores[$i];

                    }
                    else{
                        $in.=$campos[$i].'='.$valores[$i].$u;
                    }                
                }
                $comp2 = ' AND ct_reino.idReino = especie.idReino 
                          AND ct_grupo.idGrupo = especie.idGrupo 
                          AND ct_familia.idFamilia = especie.idFamilia 
                          AND ct_genero.idGenero = especie.idGenero 
                          AND ct_categoriaRiesgo.idCategoriaRiesgo = especie.idCategoriaRiesgo';
                $complemento.= $in.') AND especieEntidad.idEntidad = '.$idEntidad.$comp2;
            }            
        }
        else{ //Filtro sin Entidad
            $consulta = 'SELECT especie.*, ct_reino.nomReino, ct_grupo.nomGrupo,
                        ct_familia.nomFamilia, ct_genero.nomGenero,ct_categoriaRiesgo.categoriaRiesgo 
                        FROM especie
                        JOIN ct_reino 
                        JOIN ct_genero JOIN ct_familia
                        JOIN ct_grupo JOIN ct_categoriaRiesgo 
                        WHERE ';                   
            if($request->all() == null){
                $msg = 'No se seleccionaron parametros de búsqeuda';
                return response()->json($msg,400);
            }
            else{
                if(count($request->all()) == 1){
                    $u = '';
                }
                elseif(count($request->all()) > 1){
                    $u = ' AND ';                
                }
                $campos = array_keys($request->all());
                $valores = array_values($request->all());
                $n  = sizeof($request->all());
                for($i=0; $i<=$n-1; $i++){
                    if($i == $n-1){
                        $complemento.='especie.'.$campos[$i].'='.$valores[$i];

                    }
                    else{
                        $complemento.='especie.'.$campos[$i].'='.$valores[$i].$u;
                    }                
                }
                $comp2 = ' AND ct_reino.idReino = especie.idReino 
                          AND ct_grupo.idGrupo = especie.idGrupo 
                          AND ct_familia.idFamilia = especie.idFamilia 
                          AND ct_genero.idGenero = especie.idGenero 
                          AND ct_categoriaRiesgo.idCategoriaRiesgo = especie.idCategoriaRiesgo';
                $complemento.=$comp2;
            }
        }                
        $qry = $consulta.$complemento.';';
        $result = ejecuta($qry);
        //dd($result);
        $cad = View::make("nom059.listaEspecies",['especies'=>$result])->render();        
        $response = [
            'vista'=>$cad
        ];
        return response()->json($response,200);
        //return datatables()->of($result)->make(true);
    }

    // Lista esoecies dada una entidad ///
    public function listaEspecieByEntidad($idEntidad){
        //dd($id);
        $consulta = "SELECT especie.*, especieEntidad.idEntidad, ct_entidad.nomEntidad,
                     ct_reino.nomReino, ct_grupo.nomGrupo,
                     ct_familia.nomFamilia, ct_genero.nomGenero,ct_categoriaRiesgo.categoriaRiesgo
                     FROM especieEntidad JOIN especie 
                     JOIN ct_entidad 
                     JOIN ct_reino 
                     JOIN ct_genero JOIN ct_familia
                     JOIN ct_grupo JOIN ct_categoriaRiesgo
                     WHERE especieEntidad.idEspecie = especie.idEspecie
                     AND especieEntidad.idEntidad = ct_entidad.idEntidad 
                     AND especieEntidad.idEntidad = $idEntidad
                     AND ct_reino.idReino = especie.idReino 
                     AND ct_grupo.idGrupo = especie.idGrupo 
                     AND ct_familia.idFamilia = especie.idFamilia 
                     AND ct_genero.idGenero = especie.idGenero 
                     AND ct_categoriaRiesgo.idCategoriaRiesgo = especie.idCategoriaRiesgo;";
        $result = ejecuta($consulta);
        //dd($result);
        $cad = View::make("nom059.listaEspecies",['especies'=>$result])->render();        
        $response = [
            'vista'=>$cad
        ];
        return response()->json($response,200);
    }

    /// DEtalles de la especie ///
    public function detallesEspecie($id){
        //dd($id);
        $consulta = "SELECT especie.*, ct_reino.nomReino, ct_grupo.nomGrupo,
                        ct_familia.nomFamilia, ct_genero.nomGenero,ct_categoriaRiesgo.categoriaRiesgo,
                        ct_categoriaRiesgo.descripcion 
                        FROM especie
                        JOIN ct_reino 
                        JOIN ct_genero JOIN ct_familia
                        JOIN ct_grupo JOIN ct_categoriaRiesgo 
                        WHERE especie.idEspecie = $id
                        AND ct_reino.idReino = especie.idReino 
                        AND ct_grupo.idGrupo = especie.idGrupo 
                        AND ct_familia.idFamilia = especie.idFamilia 
                        AND ct_genero.idGenero = especie.idGenero 
                        AND ct_categoriaRiesgo.idCategoriaRiesgo = especie.idCategoriaRiesgo;";
        $result = ejecuta($consulta);
        $consultaEnt = "SELECT especieEntidad.idEspecie, especieEntidad.idEntidad, ct_entidad.nomEntidad
                        FROM especieEntidad JOIN especie 
                        JOIN ct_entidad 
                        WHERE especieEntidad.idEspecie = especie.idEspecie 
                        AND especieEntidad.idEntidad = ct_entidad.idEntidad 
                        AND especie.idEspecie = $id";
        $resultEnt = ejecuta($consultaEnt);
        $infoEspecie = [
            'especie'=>$result,
            'entidades'=>$resultEnt
        ];
        //dd($infoEspecie['especie'][0]);
        $cad = View::make("nom059.infoEspecie",['infoEspecie'=>$infoEspecie])->render();
        $response = [
            'especie'=>$result[0]->especie,
            'vista'=>$cad
        ];        
        return response()->json($response,200);
    }
    
}
