<?php
use App\EspecieEntidad;
use App\Entidad;

/// Ejecuta la consulta del filtro ///
function ejecuta($qry){
    $result = collect(DB::connection('mysql')->select($qry));
    return $result;    
}

/// Obtinene las entidades ///
function getEntidades($ids){
    $idsEntidad = [];
    $cvesArr = [];
    //dd($ids);
    foreach($ids as $id){
        $entidades = EspecieEntidad::where('idEspecie', $id)->get();
        foreach($entidades as $entidad){
            array_push($idsEntidad,$entidad->idEntidad);
        }        
    }
    $idsEntidad = array_unique($idsEntidad);
    $idsEntidad = array_filter($idsEntidad);
    if(($key = array_search(33, $idsEntidad)) != false){
        unset($idsEntidad[$key]);
    }
    foreach($idsEntidad as $id){
        $entidad = Entidad::select('cveEntidad')->where('idEntidad',$id)->first();        
        array_push($cvesArr,$entidad->cveEntidad);
    }
    //dd($cvesArr);
    return $cvesArr;
}

?>