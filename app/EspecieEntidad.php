<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class EspecieEntidad extends Model
{
    protected $guarded = [];
    protected $table = 'especieEntidad';
    protected $connection = 'mysql';
    protected $primaryKey = 'idEspecieEntidad';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function entidadRel(){
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }
    public function especieRel(){
        return $this->belongsTo('App\Especie', 'idEspecie');
    }

    ///////////////////////
    ///*** Funciones ***///
    ////////////////////// 
    static public function getEntidadesByEspecie($id){
        $entidades = EspecieEntidad::with(['entidadRel'])->where('idEspecie',$id)->get();        
        return $entidades;
    }
    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
