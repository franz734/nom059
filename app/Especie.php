<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Especie extends Model
{
    protected $guarded = [];
    protected $table = 'especie';
    protected $connection = 'mysql';
    protected $primaryKey = 'idEspecie';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function reinoRel(){
        return $this->belongsTo('App\Reino', 'idReino');
    }

    ///////////////////////
    ///*** Funciones ***///
    ////////////////////// 
    static public function getEspeciesByReino($id){
        $especies = Especie::select('idEspecie', 'especie', 'idFamilia', 'idGenero', 'idCategoriaRiesgo', 'idReino', 'idGrupo')
                            ->where('idReino', $id)->get();        
        return $especies;
    }
    static public function getEspeciesByGrupo($id){
        $especies = Especie::select('idEspecie', 'especie', 'idFamilia', 'idGenero', 'idCategoriaRiesgo', 'idReino', 'idGrupo')
                            ->where('idGrupo', $id)->get();
        return $especies;
    }
    static public function getEspeciesByFamilia($id){
        $especies = Especie::select('idEspecie', 'especie', 'idFamilia', 'idGenero', 'idCategoriaRiesgo', 'idReino', 'idGrupo')
                            ->where('idFamilia', $id)->get();
        return $especies;
    }
    static public function getEspeciesByGenero($id){
        $especies = Especie::select('idEspecie', 'especie', 'idFamilia', 'idGenero', 'idCategoriaRiesgo', 'idReino', 'idGrupo')
                            ->where('idGenero', $id)->get();
        return $especies;
    }
    static public function getEspeciesByCategoriaRiesgo($param){
        //dd($param);
        $idCategoriaRiesgo = $param['riesgo'];
        $idReino = $param['reino'];
        $idGrupo = $param['grupo'];
        $idFamilia = $param['familia'];
        $idGenero = $param['genero'];
        $especies = Especie::select('idEspecie', 'especie', 'idFamilia', 'idGenero', 'idCategoriaRiesgo')
                            ->where('idCategoriaRiesgo', $idCategoriaRiesgo)
                            ->where('idReino',$idReino)
                            ->where('idGrupo', $idGrupo)
                            ->where('idFamilia',$idFamilia)
                            ->where('idGenero',$idGenero)
                            ->get();
        return $especies;
    }

    /////////////////////
    ///*** Metodos ***///getEspeciesByRiesgo
    ////////////////////
}
