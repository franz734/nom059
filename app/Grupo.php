<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Grupo extends Model
{
    protected $guarded = [];
    protected $table = 'ct_grupo';
    protected $connection = 'mysql';
    protected $primaryKey = 'idGrupo';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function reinoRel(){
        return $this->belongsTo('App\Reino', 'idReino');
    }

    ///////////////////////
    ///*** Funciones ***///
    ////////////////////// 
    static public function getGruposByReino($id){
        $grupos = Grupo::where('idReino',$id)->orderBy('nomGrupo','ASC')->get();
        return $grupos;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
