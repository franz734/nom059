<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Genero extends Model
{
    protected $guarded = [];
    protected $table = 'ct_genero';
    protected $connection = 'mysql';
    protected $primaryKey = 'idGenero';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    ////////////////////// 
    static public function getGenerosByReino($ids){
        $generos = Genero::whereIn('idGenero',$ids)->get();
        return $generos;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
