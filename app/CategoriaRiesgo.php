<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaRiesgo extends Model
{
    protected $guarded = [];
    protected $table = 'ct_categoriaRiesgo';
    protected $connection = 'mysql';
    protected $primaryKey = 'idCategoriaRiesgo';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    ////////////////////// 
    static public function getCategoriasByReino($ids){
        $generos = CategoriaRiesgo::whereIn('idCategoriaRiesgo',$ids)->get();
        return $generos;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
