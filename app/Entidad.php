<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Entidad extends Model
{
    protected $guarded = [];
    protected $table = 'ct_entidad';
    protected $connection = 'mysql';
    protected $primaryKey = 'idEntidad';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    ////////////////////// 

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
