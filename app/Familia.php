<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Familia extends Model
{
    protected $guarded = [];
    protected $table = 'ct_familia';
    protected $connection = 'mysql';
    protected $primaryKey = 'idFamilia';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    ////////////////////// 
    static public function getFamiliasByReino($ids){
        //dd($ids);
        $familias = Familia::whereIn('idFamilia', $ids)->get();
        return $familias;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
