<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Reino extends Model
{
    protected $guarded = [];
    protected $table = 'ct_reino';
    protected $connection = 'mysql';
    protected $primaryKey = 'idReino';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    ////////////////////// 

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
