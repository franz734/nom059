function initMapa(){
    // Mapa dinamico //
    /* State Names */    
    var state_names = new Array();
    var state_class = new Array();
        state_names[1]="Aguascalientes";state_names[2]="Baja California";state_names[3]="Baja California Sur";state_names[4]="Campeche";state_names[5]="Coahuila";state_names[6]="Colima";state_names[7]="Chiapas";state_names[8]="Chihuahua";state_names[9]="Distrito Federal";state_names[10]="Durango";state_names[11]="Guanajuato";state_names[12]="Guerrero";state_names[13]="Hidalgo";state_names[14]="Jalisco";state_names[15]="Estado de M&eacute;xico";state_names[16]="Michoac&aacute;n";state_names[17]="Morelos";state_names[18]="Nayarit";state_names[19]="Nuevo Le&oacute;n";state_names[20]="Oaxaca";state_names[21]="Puebla";state_names[22]="Quer&eacute;taro";state_names[23]="Quintana roo";state_names[24]="San Luis Potos&iacute;";state_names[25]="Sinaloa";state_names[26]="Sonora";state_names[27]="Tabasco";state_names[28]="Tamaulipas";state_names[29]="Tlaxcala";state_names[30]="Veracruz";state_names[31]="Yucat&aacute;n";state_names[32]="Zacatecas";
        state_class[1]="AGU";state_class[2]="BCN";state_class[3]="BCS";state_class[4]="CAM";state_class[5]="COA";state_class[6]="COL";state_class[7]="CHP";state_class[8]="CHH";state_class[9]="DIF";state_class[10]="DUR";state_class[11]="GUA";state_class[12]="GRO";state_class[13]="HID";state_class[14]="JAL";state_class[15]="MEX";state_class[16]="MIC";state_class[17]="MOR";state_class[18]="NAY";state_class[19]="NLE";state_class[20]="OAX";state_class[21]="PUE";state_class[22]="QUE";state_class[23]="ROO";state_class[24]="SLP";state_class[25]="SIN";state_class[26]="SON";state_class[27]="TAB";state_class[28]="TAM";state_class[29]="TLA";state_class[30]="VER";state_class[31]="YUC";state_class[32]="ZAC";
    $(function () {
        $('.listaEdos').mouseover(function(e) {                
            $($(this).data('parent-map')).mouseover();
        }).mouseout(function(e) {                
            $($(this).data('parent-map')).mouseout();                    
        }).click(function(e) { 
            e.preventDefault(); 
            $($(this).data('parent-map')).click(); 
        });

    
        $('.area').hover(function () {
            var id_estado = $(this).data('id-estado');
            var meid = $(this).attr('id');
            $('#edo').html(state_names[id_estado]);                
            $('#letras'+meid).addClass('listaEdosHover');
            $('.escudo').addClass('escudo_img');
            if(last_selected_id_estado!==null){
                $('.escudo').removeClass(state_class[last_selected_id_estado]);
            }
            $('.escudo').addClass(meid);
        }).mouseout(function () {
            var meid = $(this).attr('id');
            $('#letras'+meid).removeClass('listaEdosHover');
            $('.escudo').removeClass(meid);
            if(last_selected_id_estado!==null){
                $('#edo').html(state_names[last_selected_id_estado]);
                $('.escudo').addClass(state_class[last_selected_id_estado]);
            }else{                    
                $('#edo').html("&nbsp;");
                $('.escudo').removeClass('escudo_img');
                //$('.escudo').attr('class','escudo');
            }
        });
        //$('#map_ID').imageMapResize();//funciona perfectamente
        var areaLastClicked=null;
        var last_selected_id_estado=null;
        $('.area').click(function (e) {
                e.preventDefault();
                var $area = $(this);
                //console.log($area)
                var meid = $area.attr('id');
                //console.log(meid)
                //$('.area').mouseout();
                var data = $area.data('maphilight') || {};    
                //console.log(data);                
                if(areaLastClicked!==null){                        
                    var lastData = areaLastClicked.data('maphilight') || {};
                    lastData.alwaysOn=false;
                    $('#letras'+areaLastClicked.attr('id')).removeClass('listaEdosActive');
                    $('.escudo').removeClass(state_class[last_selected_id_estado]);
                }
                $('#letras'+meid).addClass('listaEdosActive');
                areaLastClicked=$area;
                //data.alwaysOn = !data.alwaysOn;
                data.alwaysOn = true;
                //$(this).data('maphilight', data).trigger('alwaysOn.maphilight');
                last_selected_id_estado = $(this).data('id-estado');
                cargarEstado(last_selected_id_estado);
                return false;
        });

        $('.map').maphilight({ fade: true, strokeColor: '9f2241', fillColor: '9f2241', fillOpacity: 0.3 });//funciona, pero no cuando se redimienciona la imagen (cuando se cambia el estylo de la img con widt o height) 
    });
}
function initTabla(){
    $('#example').DataTable( {
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[0]+' '+data[1];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll()
            }
        }
    } );
}

function cargarEstado(id_estado){
    //que quiero hacercuando se seleccione un estado //
    console.log('Se selecciono el id_estado: '+id_estado);
    $.ajax({
        url: "/nom059/listaEspecieByEntidad/"+id_estado,
        type: 'GET',
        cache:false,
    })
    .done(function(result) {
        //console.log(result)        
        var vista = result.vista;
            $('.list').empty()
            $('.list').append(vista);
            $('#tableEspecies').DataTable( { 
                language:spanish,               
                responsive: {
                    //details:false
                    details: {
                        /* display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return data[0];
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll() */
                    }
                }
            } );                                                                                    
                             

     })
     .fail(function(result) {
         console.log(result)
     })
     .always(function() {
         console.log('complete');
     });
    //window.location = '/sintesisInformativa/entidad/'+id_estado;
}

var spanish = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    '<i class="material-icons miTi">chevron_left</i>',
        "sLast":     '<i class="material-icons miTi">chevron_right</i>',
        "sNext":     '<i class="material-icons miTi">chevron_right</i>',
        "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "copy": "Copiar",
        "colvis": "Visibilidad"
    }
  }

  let selReino;
  let selGrupo;
  let selFamilia;
  let selGenero;
  let selEspecie;
  let selRiesgo;
  let selEntidad;
  let mapa;

// Ready //
$( document ).ready(function() {    
    
    initMapa();
    $('#reino').select2();
    $('#grupo').select2();
    $('#familia').select2();
    $('#genero').select2();
    $('#riesgo').select2();
    $('#especie').select2();
    $('#entidad').select2();
    selReino = $('#reino').clone();
    selGrupo = $('#grupo').clone();
    selFamilia = $('#familia').clone();
    selGenero = $('#genero').clone();
    selEspecie = $('#especie').clone();
    selRiesgo = $('#riesgo').clone();
    selEntidad = $('#entidad').clone();
    //mapa = $('.miMap').clone();
    /* console.log(selReino[0].options);
    console.log(selGrupo[0].options);
    console.log(selFamilia[0].options);
    console.log(selGenero[0].options);
    console.log(selEspecie[0].options);    
    console.log(selRiesgo[0].options);
    console.log(selEntidad[0].options); */
    //Select Reino
    $('#reino').change(function(){
        $('#grupo').val('def').trigger('change');
        $('#familia').val('def').trigger('change');
        $('#genero').val('def').trigger('change');
        $('#riesgo').val('def').trigger('change');
        $('#especie').val('def').trigger('change');        
        $('.miMap').empty();
        $('.miMap').html('<div class="d-flex justify-content-center"><div class="spinner-border text-muted"></div></div>');
        $('.list').empty();

        /* $('#entidad').select2('destroy');
        $('#entidad').select2(); */

        var especieArr = [];
        var grupoArr = [];
        var familiaArr = [];
        var generoArr = [];
        var riesgoArr = [];
        var especieArr = [];
        var reino = $(this).val();
        $.ajax({
            url: "/nom059/listaCombo/"+reino,
            type: 'GET',
            cache:false,
        })
        .done(function(result) {
            //console.log(result)
            var grupos = result.grupos;
            var familias = result.familias;
            var generos = result.generos;
            var riesgos = result.riesgos;
            var especies = result.especies;
            var vista = result.vista;
            //return false;
            //Grupos//
            for(const i in grupos){
                if(grupos[i] != null){
                    grupoArr.push({
                         id:grupos[i].idGrupo,
                         nombre:grupos[i].nomGrupo,                                                 
                    })
                }                 
           }
           var cadCom = '<option disabled selected value="def">Seleccione una opción</option>'; 
           for(const i in grupoArr){
              cadCom += '<option value="'+grupoArr[i].id+'">'+grupoArr[i].nombre
                   + '</option>';  
           }
           //Familias//
           for(const i in familias){
                if(familias[i] != null){
                    familiaArr.push({
                         id:familias[i].idFamilia,
                         nombre:familias[i].nomFamilia,                                                 
                    })
                }                 
            }
            var cadComF = '<option disabled selected value="def">Seleccione una opción</option>'; 
            for(const i in familiaArr){
               cadComF += '<option value="'+familiaArr[i].id+'">'+familiaArr[i].nombre
                    + '</option>';  
            }
            //Generos//
           for(const i in generos){
                if(generos[i] != null){
                    generoArr.push({
                         id:generos[i].idGenero,
                         nombre:generos[i].nomGenero,                                                 
                    })
                }                 
            }
            var cadComG = '<option disabled selected value="def">Seleccione una opción</option>'; 
            for(const i in generoArr){
               cadComG += '<option value="'+generoArr[i].id+'">'+generoArr[i].nombre
                    + '</option>';  
            }
            //Riesgos//
            for(const i in riesgos){
                if(riesgos[i] != null){
                    riesgoArr.push({
                         id:riesgos[i].idCategoriaRiesgo,
                         nombre:riesgos[i].categoriaRiesgo,                                                 
                    })
                }                 
           }
           var cadComR = '<option disabled selected value="def">Seleccione una opción</option>'; 
           for(const i in riesgoArr){
              cadComR += '<option value="'+riesgoArr[i].id+'">'+riesgoArr[i].nombre
                   + '</option>';  
           }
            //Especiess//
           for(const i in especies){
                if(especies[i] != null){
                    especieArr.push({
                         id:especies[i].idEspecie,
                         nombre:especies[i].especie,                                                 
                    })
                }                 
            }
            var cadComE = '<option disabled selected value="def">Seleccione una opción</option>'; 
            for(const i in especieArr){
               cadComE += '<option value="'+especieArr[i].id+'">'+especieArr[i].nombre
                    + '</option>';  
            }
            //console.log(cadCom); 
           $('.miMap').empty();
           $('.miMap').html(vista);    
           initMapa();         
           $('#grupo').empty()
           $('#grupo').append(cadCom);
           $('#familia').empty()
           $('#familia').append(cadComF);
           $('#genero').empty()
           $('#genero').append(cadComG);
           $('#riesgo').empty()
           $('#riesgo').append(cadComR);
           $('#especie').empty()
           $('#especie').append(cadComE);           
 
         })
         .fail(function(result) {
             console.log(result)
         })
         .always(function() {
             console.log('complete');
         });
        
    });
    //Select Grupo
    $('#grupo').change(function(){        
        $('#familia').val('def').trigger('change');
        $('#genero').val('def').trigger('change');
        $('#riesgo').val('def').trigger('change');
        $('#especie').val('def').trigger('change');
        $('.miMap').empty();
        $('.miMap').html('<div class="d-flex justify-content-center"><div class="spinner-border text-muted"></div></div>');
        $('.list').empty();
        
        var familiaArr = [];
        var generoArr = [];
        var riesgoArr = [];
        var especieArr = [];
        var grupo = $(this).val();
        if(grupo !== null){
            //console.log(grupo);
            $.ajax({
                url: "/nom059/listaComboByGrupo/"+grupo,
                type: 'GET',
                cache:false,
            })
            .done(function(result) {
                //console.log(result)
                //return false;                
                var familias = result.familias;
                var generos = result.generos;
                var riesgos = result.riesgos;
                var especies = result.especies;    
                var vista = result.vista;                            
               //Familias//
               for(const i in familias){
                    if(familias[i] != null){
                        familiaArr.push({
                             id:familias[i].idFamilia,
                             nombre:familias[i].nomFamilia,                                                 
                        })
                    }                 
                }
                var cadComF = '<option disabled selected value="def">Seleccione una opción</option>'; 
                for(const i in familiaArr){
                   cadComF += '<option value="'+familiaArr[i].id+'">'+familiaArr[i].nombre
                        + '</option>';  
                }
                //Generos//
               for(const i in generos){
                    if(generos[i] != null){
                        generoArr.push({
                             id:generos[i].idGenero,
                             nombre:generos[i].nomGenero,                                                 
                        })
                    }                 
                }
                var cadComG = '<option disabled selected value="def">Seleccione una opción</option>'; 
                for(const i in generoArr){
                   cadComG += '<option value="'+generoArr[i].id+'">'+generoArr[i].nombre
                        + '</option>';  
                }
                //Riesgos//
                 for(const i in riesgos){
                     if(riesgos[i] != null){
                         riesgoArr.push({
                              id:riesgos[i].idCategoriaRiesgo,
                              nombre:riesgos[i].categoriaRiesgo,                                                 
                         })
                     }                 
                }
                var cadComR = '<option disabled selected value="def">Seleccione una opción</option>'; 
                for(const i in riesgoArr){
                   cadComR += '<option value="'+riesgoArr[i].id+'">'+riesgoArr[i].nombre
                        + '</option>';  
                }
                //Especiess//
               for(const i in especies){
                    if(especies[i] != null){
                        especieArr.push({
                             id:especies[i].idEspecie,
                             nombre:especies[i].especie,                                                 
                        })
                    }                 
                }
                var cadComE = '<option disabled selected value="def">Seleccione una opción</option>'; 
                for(const i in especieArr){
                   cadComE += '<option value="'+especieArr[i].id+'">'+especieArr[i].nombre
                        + '</option>';  
                }
                //console.log(cadCom);   
                $('.miMap').empty();
                $('.miMap').html(vista);    
                initMapa();
                $('#reino').select2('destroy');
                $('#reino').val(especies[0].idReino)/* .trigger('change').off().select2().on() */;
                $('#reino').select2();            
                $('#familia').empty()
                $('#familia').append(cadComF);
                $('#genero').empty()
                $('#genero').append(cadComG);
                $('#riesgo').empty()
                $('#riesgo').append(cadComR);
                $('#especie').empty()
                $('#especie').append(cadComE);           
     
             })
             .fail(function(result) {
                 console.log(result)
             })
             .always(function() {
                 console.log('complete');
             });
        }
        
    });
    //Select Familia
    $('#familia').change(function(){                
        $('#genero').val('def').trigger('change');
        $('#riesgo').val('def').trigger('change');        
        $('#especie').val('def').trigger('change');
        $('.miMap').empty();
        $('.miMap').html('<div class="d-flex justify-content-center"><div class="spinner-border text-muted"></div></div>');
        $('.list').empty();
                
        var generoArr = [];
        var riesgoArr = [];
        var especieArr = [];
        var familia = $(this).val();
        if(familia !== null){
            //console.log(grupo);
            $.ajax({
                url: "/nom059/listaComboByFamilia/"+familia,
                type: 'GET',
                cache:false,
            })
            .done(function(result) {
                //console.log(result)
                //return false;                                
                var generos = result.generos;
                var riesgos = result.riesgos;
                var especies = result.especies;   
                var vista = result.vista;                                            
                //Generos//
               for(const i in generos){
                    if(generos[i] != null){
                        generoArr.push({
                             id:generos[i].idGenero,
                             nombre:generos[i].nomGenero,                                                 
                        })
                    }                 
                }
                var cadComG = '<option disabled selected value="def">Seleccione una opción</option>'; 
                for(const i in generoArr){
                   cadComG += '<option value="'+generoArr[i].id+'">'+generoArr[i].nombre
                        + '</option>';  
                }
                //Riesgos//
                for(const i in riesgos){
                    if(riesgos[i] != null){
                        riesgoArr.push({
                             id:riesgos[i].idCategoriaRiesgo,
                             nombre:riesgos[i].categoriaRiesgo,                                                 
                        })
                    }                 
               }
               var cadComR = '<option disabled selected value="def">Seleccione una opción</option>'; 
               for(const i in riesgoArr){
                  cadComR += '<option value="'+riesgoArr[i].id+'">'+riesgoArr[i].nombre
                       + '</option>';  
               }
                //Especiess//
               for(const i in especies){
                    if(especies[i] != null){
                        especieArr.push({
                             id:especies[i].idEspecie,
                             nombre:especies[i].especie,                                                 
                        })
                    }                 
                }
                var cadComE = '<option disabled selected value="def">Seleccione una opción</option>'; 
                for(const i in especieArr){
                   cadComE += '<option value="'+especieArr[i].id+'">'+especieArr[i].nombre
                        + '</option>';  
                }
                //console.log(cadCom);   
                $('.miMap').empty();
                $('.miMap').html(vista);    
                initMapa();
                $('#grupo').select2('destroy');
                $('#grupo').val(especies[0].idGrupo)/* .trigger('change').off().select2().on() */;
                $('#grupo').select2();
                $('#reino').select2('destroy');
                $('#reino').val(especies[0].idReino)/* .trigger('change').off().select2().on() */;
                $('#reino').select2();                           
                $('#genero').empty()
                $('#genero').append(cadComG);
                $('#riesgo').empty()
                $('#riesgo').append(cadComR);
                $('#especie').empty()
                $('#especie').append(cadComE);           
     
             })
             .fail(function(result) {
                 console.log(result)
             })
             .always(function() {
                 console.log('complete');
             });
        }
        
    });
    //Select Genero
    $('#genero').change(function(){  
        $('#riesgo').val('def').trigger('change');                      
        $('#especie').val('def').trigger('change');
        $('.miMap').empty();
        $('.miMap').html('<div class="d-flex justify-content-center"><div class="spinner-border text-muted"></div></div>');
        $('.list').empty();
        
        var riesgoArr = [];
        var especieArr = [];
        var genero = $(this).val();
        if(genero !== null){
            //console.log(grupo);
            $.ajax({
                url: "/nom059/listaComboByGenero/"+genero,
                type: 'GET',
                cache:false,
            })
            .done(function(result) {
                //console.log(result)
                //return false;      
                var riesgos = result.riesgos;                                         
                var especies = result.especies; 
                var vista = result.vista;
                //Riesgos//
                for(const i in riesgos){
                    if(riesgos[i] != null){
                        riesgoArr.push({
                             id:riesgos[i].idCategoriaRiesgo,
                             nombre:riesgos[i].categoriaRiesgo,                                                 
                        })
                    }                 
               }
               var cadComR = '<option disabled selected value="def">Seleccione una opción</option>'; 
               for(const i in riesgoArr){
                  cadComR += '<option value="'+riesgoArr[i].id+'">'+riesgoArr[i].nombre
                       + '</option>';  
               }                                                              
                //Especiess//
               for(const i in especies){
                    if(especies[i] != null){
                        especieArr.push({
                             id:especies[i].idEspecie,
                             nombre:especies[i].especie,                                                 
                        })
                    }                 
                }
                var cadComE = '<option disabled selected value="def">Seleccione una opción</option>'; 
                for(const i in especieArr){
                   cadComE += '<option value="'+especieArr[i].id+'">'+especieArr[i].nombre
                        + '</option>';  
                }
                //console.log(cadCom); 
                $('.miMap').empty();
                $('.miMap').html(vista);    
                initMapa();
                $('#familia').select2('destroy');
                $('#familia').val(especies[0].idFamilia)/* .trigger('change').off().select2().on() */;
                $('#familia').select2();
                $('#grupo').select2('destroy');
                $('#grupo').val(especies[0].idGrupo)/* .trigger('change').off().select2().on() */;
                $('#grupo').select2();
                $('#reino').select2('destroy');
                $('#reino').val(especies[0].idReino)/* .trigger('change').off().select2().on() */;
                $('#reino').select2();
                $('#riesgo').empty()
                $('#riesgo').append(cadComR);                                            
                $('#especie').empty()
                $('#especie').append(cadComE);           
     
             })
             .fail(function(result) {
                 console.log(result)
             })
             .always(function() {
                 console.log('complete');
             });
        }
        
    });
    //Select Riesgo
    /* $('#riesgo').change(function(){                                
        $('#especie').val('def').trigger('change');
                
        var especieArr = [];
        var reino  = $('#reino').val();
        var grupo = $('#grupo').val();
        var familia  = $('#familia').val();
        var genero =  $('#genero').val();
        var riesgo = $(this).val();                        
        if(riesgo !== null){
            //console.log(grupo);
            $.ajax({
                url: "/nom059/listaComboByRiesgo/"+riesgo+'/'+reino+'/'+grupo+'/'+familia+'/'+genero,
                type: 'GET',
                cache:false,
            })
            .done(function(result) {
                console.log(result)
                //return false;                                                              
                var especies = result.especies;                                                                        
                //Especiess//
               for(const i in especies){
                    if(especies[i] != null){
                        especieArr.push({
                             id:especies[i].idEspecie,
                             nombre:especies[i].especie,                                                 
                        })
                    }                 
                }
                var cadComE = '<option disabled selected value="def">Seleccione una opción</option>'; 
                for(const i in especieArr){
                   cadComE += '<option value="'+especieArr[i].id+'">'+especieArr[i].nombre
                        + '</option>';  
                }
                //console.log(cadCom);                                                            
               $('#especie').empty()
               $('#especie').append(cadComE);           
     
             })
             .fail(function(result) {
                 console.log(result)
             })
             .always(function() {
                 console.log('complete');
             });
        }
        
    }); */
    //Select Especie
    $('#especie').change(function(){                                
        $('#entidad').val('def').trigger('change');
        $('#riesgo').val('def').trigger('change');        
        $('.miMap').empty();
        $('.miMap').html('<div class="d-flex justify-content-center"><div class="spinner-border text-muted"></div></div>');
        $('.list').empty();
        
        var riesgoArr = [];
        var entidadArr = [];
        var especie = $(this).val();                        
        if(especie !== null){
            //console.log(grupo);
            $.ajax({
                url: "/nom059/listaComboByEspecie/"+especie,
                type: 'GET',
                cache:false,
            })
            .done(function(result) {
                //console.log(result)
                //return false;                                                              
                var entidades = result.entidades; 
                var riesgos = result.riesgos; 
                var especie  =result.especie;
                var vista = result.vista;
                //Riesgos//
                for(const i in riesgos){
                    if(riesgos[i] != null){
                        riesgoArr.push({
                             id:riesgos[i].idCategoriaRiesgo,
                             nombre:riesgos[i].categoriaRiesgo,                                                 
                        })
                    }                 
               }
               var cadComR = '<option disabled selected value="def">Seleccione una opción</option>'; 
               for(const i in riesgoArr){
                  cadComR += '<option value="'+riesgoArr[i].id+'">'+riesgoArr[i].nombre
                       + '</option>';  
               }                                                                      
                //Especiess//
               for(const i in entidades){
                    if(entidades[i] != null){
                        entidadArr.push({
                             id:entidades[i].idEntidad,
                             nombre:entidades[i].nomEntidad,                                                 
                        })
                    }                 
                }
                var cadComEn = '<option disabled selected value="def">Seleccione una opción</option>'; 
                for(const i in entidadArr){
                   cadComEn += '<option value="'+entidadArr[i].id+'">'+entidadArr[i].nombre
                        + '</option>';  
                }
                //console.log(cadCom); 
                //$('.map').html('');
                $('.miMap').empty();
                $('.miMap').html(vista);    
                initMapa();
                $('#reino').select2('destroy');
                $('#reino').val(especie.idReino)/* .trigger('change').off().select2().on() */;
                $('#reino').select2();
                $('#grupo').select2('destroy');
                $('#grupo').val(especie.idGrupo)/* .trigger('change').off().select2().on() */;
                $('#grupo').select2();
                $('#familia').select2('destroy');
                $('#familia').val(especie.idFamilia)/* .trigger('change').off().select2().on() */;
                $('#familia').select2();
                $('#genero').select2('destroy');
                $('#genero').val(especie.idGenero)/* .trigger('change').off().select2().on() */;
                $('#genero').select2();
                $('#riesgo').empty()
                $('#riesgo').append(cadComR);                                                          
                $('#entidad').empty()
                $('#entidad').append(cadComEn);           
     
             })
             .fail(function(result) {
                 console.log(result)
             })
             .always(function() {
                 console.log('complete');
             });
        }
        
    });
    //Select Entidad
    $('#entidad').change(function(){
        $('.list').empty();
        var entidad = $(this).val();
        if(entidad !== null){
            $.ajax({
                url: "/nom059/listaComboByEntidad/"+entidad,
                type: 'GET',
                cache:false,
            })
            .done(function(result) {
                //console.log(result)
                //return false;                                                                          
                var vista = result.vista;                                                                                    
                $('.miMap').empty();
                $('.miMap').html(vista);    
                initMapa();                      
     
             })
             .fail(function(result) {
                 console.log(result)
             })
             .always(function() {
                 console.log('complete');
             });
        }
        

    });

    $('#btnLimpia').click(function(e){        
        //alert('hola');

        //console.log(selGrupo[0].options); 
        $('#reino').empty();
        $('#reino').append(selReino[0].options);
        selReino = $('#reino').clone();
        $('#reino').select2('destroy');
        $('#reino').val('def');
        $('#reino').select2();
        $('#grupo').empty();
        $('#grupo').append(selGrupo[0].options);
        selGrupo = $('#grupo').clone();      
        $('#grupo').select2('destroy');
        $('#grupo').val('def');
        $('#grupo').select2(); 
        $('#familia').empty();
        $('#familia').append(selFamilia[0].options);
        selFamilia = $('#familia').clone();      
        $('#familia').select2('destroy');
        $('#familia').val('def');
        $('#familia').select2();
        $('#genero').empty();
        $('#genero').append(selGenero[0].options);
        selGenero = $('#genero').clone();      
        $('#genero').select2('destroy');
        $('#genero').val('def');
        $('#genero').select2();
        $('#especie').empty();
        $('#especie').append(selEspecie[0].options);
        selEspecie = $('#especie').clone();      
        $('#especie').select2('destroy');
        $('#especie').val('def');
        $('#especie').select2();
        $('#riesgo').empty();
        $('#riesgo').append(selRiesgo[0].options);
        selRiesgo = $('#riesgo').clone();      
        $('#riesgo').select2('destroy');
        $('#riesgo').val('def');
        $('#riesgo').select2();  
        $('#entidad').empty();
        $('#entidad').append(selEntidad[0].options);
        selEntidad = $('#entidad').clone();      
        $('#entidad').select2('destroy');
        $('#entidad').val('def');
        $('#entidad').select2();
        $('.list').empty();
        //initMapa()
        var state_class = new Array();        
        state_class[1]="AGU";state_class[2]="BCN";state_class[3]="BCS";state_class[4]="CAM";state_class[5]="COA";state_class[6]="COL";state_class[7]="CHP";state_class[8]="CHH";state_class[9]="DIF";state_class[10]="DUR";state_class[11]="GUA";state_class[12]="GRO";state_class[13]="HID";state_class[14]="JAL";state_class[15]="MEX";state_class[16]="MIC";state_class[17]="MOR";state_class[18]="NAY";state_class[19]="NLE";state_class[20]="OAX";state_class[21]="PUE";state_class[22]="QUE";state_class[23]="ROO";state_class[24]="SLP";state_class[25]="SIN";state_class[26]="SON";state_class[27]="TAB";state_class[28]="TAM";state_class[29]="TLA";state_class[30]="VER";state_class[31]="YUC";state_class[32]="ZAC";
        //$('.map').maphilight({ alwaysOn:false });
        for(const i in state_class){
            var cad = "#"+state_class[i];
            //console.log(cad);
            $(cad).data('maphilight', {'alwaysOn':false,});
        }
        $('.map').maphilight({ alwaysOn:false });
        $('.map').maphilight({ fade: true, strokeColor: '9f2241', fillColor: '9f2241', fillOpacity: 0.3 });
        //$(cad).data('maphilight', {'alwaysOn': false});
        //$('.map').html('');
        /* $('.miMap').empty();
        $('.miMap').append(mapa);
        initMapa(); */
        
        //$('.miMap').empty();
    });

    $('#btnFilter').click(function(e){
        e.preventDefault();
        var formdata = new FormData($("#filterForm")[0]);
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/nom059/filter",
            type: 'POST',
            dataType: 'json',
            data: formdata,                
            cache:false,
            processData: false,
            contentType: false,
        })
        .done(function(result) {            
            //console.log(result)
            //initTabla();
            var vista = result.vista;
            $('.list').empty()
            $('.list').append(vista);
            $('#tableEspecies').DataTable( { 
                language:spanish,               
                responsive: {
                    //details:false
                    details: {
                        /* display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return data[0];
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll() */
                    }
                }
            } );            
            
         })
         .fail(function(result) {
            console.log('Fail');            
            //alert(result.responseJSON)
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: result.responseJSON,
                showConfirmButton: false,
                timer: 2500

              })
        })
        .always(function() {
            console.log('complete');
        });
    });    
});

//ON
$(document).on('click','#tableEspecies tbody .btnDet', function(e){
    e.preventDefault();
    var tr = $(this).closest('tr');  
    var idEspecie = tr[0].lastChild.lastChild.lastChild.childNodes[2].childNodes[0].attributes[2].value;
    //console.log(idEspecie)    
    $.ajax({
        url: "/nom059/detallesEspecie/"+idEspecie,
        type: 'GET',
        cache:false,
    })
    .done(function(result) {
        console.log(result)
        $('#descModal').modal({show:true}); 
        $('#descModalLabel').html('<h4 class="modal-title">'+result.especie+'</h4>');
        $('#descModalBody').html(result.vista);                           

     })
     .fail(function(result) {
         console.log(result)
     })
     .always(function() {
         console.log('complete');
     });
    //$('#descModal').modal({show:true});
    //$('#descModalLabel').html('<h5 class="modal-title">'+data.nomEncuesta+'</h5>');
    //$('#descModalBody').html(data.descripcion);
  });
