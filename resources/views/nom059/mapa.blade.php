@php
    use App\Entidad;
    $edosArr = [];
    $todas = Entidad::select('cveEntidad')->where('idEntidad','!=',33)->get();
    foreach($todas as $t){
        array_push($edosArr,$t->cveEntidad);
    }    
@endphp
<style>
     #content_mapa{
	/* float:right; */
	position: relative !important;
	left: 30px !important;
	top: 20px !important;
}
#map_ID{
    width: 662px !important;
}
</style>

<img id="map_ID" class="map" src="{{URL::asset('assets/images/mapa.png')}}" usemap="#mexico" title="Mexico" alt="Mexico" />
<br>      
{{-- <script src="{{URL::asset('assets/js/mapaHelp.js')}}?{{rand()}}"></script> --}}  
@foreach ($edosArr as $edo)
    <script>
        var cve = [ '<?php echo $edo;?>' ];
        //console.log((cve[0])); 
        var cad = "#"+cve[0];
        $(cad).data('maphilight', {'alwaysOn': false});
        //$(cad).data('maphilight', {'alwaysOn': true});
    </script>
@endforeach
@foreach ($idEdos as $id)
    <script>        
        var cve = [ '<?php echo $id;?>' ];
        //console.log((cve[0])); 
        var cad = "#"+cve[0];
        //$(cad).data('maphilight', {'alwaysOn': false});
        $(cad).data('maphilight', {'alwaysOn': true});                       
    </script>
@endforeach
