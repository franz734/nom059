<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>NOM 059</title>
    <!-- CSS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{URL::asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/mapa-mexico/estilos_mapa.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/datatables/responsive.dataTables.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/css/nom059.css')}}" rel="stylesheet" />
    <!-- -->
    @yield('css')
</head>