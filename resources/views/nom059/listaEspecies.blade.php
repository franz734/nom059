<br>
<div class="lista-cont" style="width: 100%">
    <table id="tableEspecies" class="display nowrap" style="width:100%">
        <thead>
            <tr>
                <th>Especie</th>  
                <th>Reino</th>  
                <th>Grupo</th>
                <th>Familia</th>
                <th>Genero</th>
                <th>Categoria de Riesgo</th>                  
                <th></th>              
            </tr>
        </thead>      
        <tbody>            
            @foreach ($especies as $especie)
                <tr>
                    <td>{{$especie->especie}}</td>
                    <td>{{$especie->nomReino}}</td>
                    <td>{{$especie->nomGrupo}}</td>
                    <td>{{$especie->nomFamilia}}</td>
                    <td>{{$especie->nomGenero}}</td>
                    <td>{{$especie->categoriaRiesgo}}</td>                       
                    <td>
                        {{-- <form action=""> --}}
                            <input hidden id="idEspecie" type="text" value="{{$especie->idEspecie}}">
                            <a type="button" class="btn btn-primary btnDet" >Detalles</a>
                        {{-- </form> --}}
                    </td>                
                </tr>
            @endforeach
        </tbody>  
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="descModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div id='descModalLabel'></div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id='descModalBody'>              
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>              
        </div>
      </div>
    </div>
</div> 
{{-- <table id="example" class="display nowrap" style="width:100%">
    <thead>
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Position</th>
            <th>Office</th>
            <th>Age</th>
            <th>Start date</th>
            <th>Salary</th>
            <th>Extn.</th>
            <th>E-mail</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Tiger</td>
            <td>Nixon</td>
            <td>System Architect</td>
            <td>Edinburgh</td>
            <td>61</td>
            <td>2011/04/25</td>
            <td>$320,800</td>
            <td>5421</td>
            <td>t.nixon@datatables.net</td>
        </tr>
    </tbody>
</table> --}}
