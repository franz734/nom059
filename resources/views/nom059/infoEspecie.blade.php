@php
    $especie = $infoEspecie['especie'][0];
    $estados = $infoEspecie['entidades'];
    //dd($estados);
    //dd($especie->sinominia);
    if(!is_null($especie->sinominia)){
        $sinominia = explode(',',$especie->sinominia);
    }
    if(!is_null($especie->nombresComunes)){
        $nomComunes = explode(',',$especie->nombresComunes);
    }
@endphp
<div class="row">
    <div class="col">
        <h5><b>Sinominía</b></h5>
        @if (!is_null($especie->sinominia))            
            <ul>
                @foreach ($sinominia as $key)
                    <li>{{$key}}</li>
                @endforeach
            </ul>
        @else
            <p style="color: red">&nbsp;&nbsp;Sin Sinominía</p>        
        @endif
    </div>
    <div class="col">
        <h5><b>Nombres Comúnes</b></h5>
        @if (!is_null($especie->nombresComunes))            
            <ul>
                @foreach ($nomComunes as $key)
                    <li>{{$key}}</li>
                @endforeach
            </ul>
        @else
            <p style="color: red">&nbsp;&nbsp;Sin Nombres Comúnes</p>        
        @endif
    </div>
</div>
<br>
<div class="row">
    <div class="col">
      <h5><b>Distribución</b></h5>
      @if ($especie->distribucion == null)
            <p>&nbsp;&nbsp;No Endémica</p>
      @else
            <p>&nbsp;&nbsp;{{$especie->distribucion}}</p>
      @endif
    </div>
    <div class="col">
        <h5><b>Categoría de Riesgo</b></h5>
        <p>&nbsp;&nbsp;{{$especie->categoriaRiesgo}} - {{$especie->descripcion}}</p>
    </div>
</div>
<br>
<div class="row">
    <div class="col">
        <h5><b>Ubicación</b></h5>
        <ul>
            @foreach ($estados as $estado)
                <li>{{$estado->nomEntidad}}</li>
            @endforeach
        </ul>
    </div>
    <div class="col">
        
    </div>
  </div>

