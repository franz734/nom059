
<nav class="navbar navbar-expand-md navbar-dark bg-light miNav">
    <a class="navbar-brand" href="#">
        <img src="{{URL::asset('assets/images/Logo_gob.png')}}" height="45px">
        {{-- <img src="{{URL::asset('assets/images/LogoMedioAmbiente-Profepa.png')}}" height="45px"> --}}
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">       
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link aNav" href="https://www.gob.mx/profepa/documentos/norma-oficial-mexicana-nom-059-semarnat-2010" target="_blank">NOM-059-SEMARNAT-2010</a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link aNav" href="">elementoB</a>
            </li> --}}
        </ul>
    </div>
</nav>