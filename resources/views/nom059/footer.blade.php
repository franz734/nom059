<!-- jQuery and JS bundle w/ Popper.js -->
<script src="{{URL::asset('assets/plugins/jquery/jquery-2.2.0.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
{{-- <script src="{{URL::asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="{{URL::asset('assets/js/nom059.js')}}?{{rand()}}"></script>
<!-- -->
@yield('scripts')